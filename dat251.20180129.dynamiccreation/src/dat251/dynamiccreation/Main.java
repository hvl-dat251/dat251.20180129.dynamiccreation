package dat251.dynamiccreation;

public class Main {
	
	public static void main(String[] args) {
		
		Car car1 = createCar("FamilyCar");
		System.out.println(car1);
		
		Car car2 = createCar("Truck");
		System.out.println(car2);
	}
	
	private static Car createCar(String type) {
		try {
			return (Car) Class.forName("dat251.dynamiccreation." + type).newInstance();
			
		} catch (Exception e) {
			return null;
		}
	}
}